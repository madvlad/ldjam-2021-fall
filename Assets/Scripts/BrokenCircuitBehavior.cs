using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrokenCircuitBehavior : MonoBehaviour
{
    public AudioClip fixedCircuit;
    private AudioSource SFXSpeak;

    private void Start() {
        SFXSpeak = GameObject.FindGameObjectWithTag("SFXSpeak").GetComponent<AudioSource>();
    }
    /// <summary>
    /// Sent when another object enters a trigger collider attached to this
    /// object (2D physics only).
    /// </summary>
    /// <param name="other">The other Collider2D involved in this collision.</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player")) {
            SFXSpeak.PlayOneShot(fixedCircuit);
            Destroy(gameObject);
        }
    }
}
