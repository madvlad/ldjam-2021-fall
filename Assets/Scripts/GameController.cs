using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
	public static GameController instance;
	public GameObject hudContainer, gameOverPanel, youWinPanel;

	public bool gamePlaying { get; private set; }

	private void Awake()
	{
		instance = this;
	}

	private void Start()
	{
		gamePlaying = false;
		BeginGame();
	}

	private void BeginGame()
	{
		gamePlaying = true;
		GameTimer.instance.BeginTimer();
	}

	public void WinGame() {
		gamePlaying = false;
		Invoke("ShowYouWinScreen", 0.5f);
	}

	public void EndGame()
	{
		gamePlaying = false;
		Invoke("ShowGameOverScreen", 0.5f);
	}

	private void ShowGameOverScreen()
	{
		gameOverPanel.SetActive(true);
		hudContainer.SetActive(false);
		Invoke("RestartGame", 5.0f);
	}

	private void ShowYouWinScreen() {
		youWinPanel.SetActive(true);
		hudContainer.SetActive(false);
		Invoke("RestartGame", 10.0f);
	}

	private void RestartGame() {
		SceneManager.LoadScene("MainMenu");
	}
}
