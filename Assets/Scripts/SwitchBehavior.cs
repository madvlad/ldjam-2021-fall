using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchBehavior : MonoBehaviour
{
    public GameObject OnSprite;
    public GameObject OffSprite;
    public PlantHealthController GameManager;
    public AudioClip SoundFX;
    private AudioSource SFXSpeak;

    private void Start() {
        SFXSpeak = GameObject.FindGameObjectWithTag("SFXSpeak").GetComponent<AudioSource>();
    }

    public void SwitchOff() {
        OnSprite.SetActive(false);
        OffSprite.SetActive(true);
    }

    public void SwitchOn() {
        OnSprite.SetActive(true);
        OffSprite.SetActive(false);
        GameManager.IncreaseRoomHealth("yellow");
        SFXSpeak.PlayOneShot(SoundFX);
    }
}
