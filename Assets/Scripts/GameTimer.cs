using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour
{
    public static GameTimer instance;

    public Text timeCounter;
    private bool timerGoing;
    private double timeRemaining;
    [SerializeField] private int minutes;
    [SerializeField] private int seconds;
    [SerializeField] private int healthDegradeTime;
    private TimeSpan timeLimit;

    private void Awake()
    {
        instance = this;
        timeLimit = new TimeSpan(0, minutes, seconds);
    }

    private void Start()
    {
        timeCounter.text = GetFormattedTime(timeLimit);
        timerGoing = false;
    }

    public void BeginTimer()
    {
        timerGoing = true;
        timeRemaining = timeLimit.TotalSeconds;

        StartCoroutine(UpdateTimer());
    }

    public void EndTimer()
    {
        timerGoing = false;
    }

    private IEnumerator UpdateTimer()
    {
        while (timerGoing)
        {
            timeRemaining -= Time.deltaTime;

            // check if time limit is reached
            if (timeRemaining <= 0)
            {
                timerGoing = false;
                timeRemaining = 0;
                GameController.instance.WinGame();
            }

            var remainingTimeSpan = TimeSpan.FromSeconds(timeRemaining);
            timeCounter.text = GetFormattedTime(remainingTimeSpan);

            yield return null;
        }
    }

    private string GetFormattedTime(TimeSpan time)
    {
        return "Time: " + time.ToString("mm':'ss'.'ff");
    }
}
