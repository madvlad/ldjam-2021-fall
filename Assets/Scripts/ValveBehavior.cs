using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValveBehavior : MonoBehaviour
{
    public GameObject BoomBooms;
    public bool IsOn = false;
    public AudioClip ChillSFX;
    public PlantHealthController HealthController;
    private AudioSource SFXSpeak;

    private void Start() {
        SFXSpeak = GameObject.FindGameObjectWithTag("SFXSpeak").GetComponent<AudioSource>();
    }

    public void Reset() {
        BoomBooms.SetActive(true);
        IsOn = true;
    }

    /// <summary>
    /// Sent when another object enters a trigger collider attached to this
    /// object (2D physics only).
    /// </summary>
    /// <param name="other">The other Collider2D involved in this collision.</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && IsOn) {
            IsOn = false;
            BoomBooms.SetActive(false);
            SFXSpeak.PlayOneShot(ChillSFX);
            HealthController.IncreaseRoomHealth("green");
        }
    }
}
