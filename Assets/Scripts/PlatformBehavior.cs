using UnityEngine;

public class PlatformBehavior : MonoBehaviour
{
    public Transform pos1 = null, pos2 = null;
    public float speed;
    public Transform startPos;
    private PlantHealthController PlantHealth;

    Vector3 nextPos;
    // Start is called before the first frame update
    void Start()
    {
        nextPos = startPos.position;
        PlantHealth = GameObject.FindGameObjectWithTag("GameManager").GetComponent<PlantHealthController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!PlantHealth.IsPlantPowered) {
            return;
        }
        if(transform.position == pos1.position)
        {
            nextPos = pos2.position;
        }
        if (transform.position == pos2.position)
        {
            nextPos = pos1.position;
        }
        transform.position = Vector3.MoveTowards(transform.position, nextPos, speed * Time.deltaTime);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(pos1.position, pos2.position);
    }
}
