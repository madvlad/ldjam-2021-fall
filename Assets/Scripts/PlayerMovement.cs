using System.Collections;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	public bool isClimbing = false;
	public bool isHolding = false;
	public bool grounded;
	public AudioClip climbingClip;
	public AudioClip jumpingClip;
	public AudioClip toxicSplashClip;
	private AudioSource SFXSpeak;

	[SerializeField] private float playerScale;
	[SerializeField] private float speed;
	private Rigidbody2D body;
	private Animator anim;

	private float gravityScaleDefault;
	private (ItemType, int) itemHeld;
	private float animationSpeed = 0;

	public void TriggerDeath()
	{
		body.gravityScale = 0;
		body.velocity = new Vector2(body.velocity.x, body.velocity.y * 0.25f);
		StartCoroutine(Die());
	}

	private IEnumerator Die()
	{
		anim.SetTrigger("death");
		SFXSpeak.PlayOneShot(toxicSplashClip);
		yield return new WaitForSeconds(1.5f);
		GameController.instance.EndGame();
	}

	public void setClimbing(bool value) {
		isClimbing = value;
		anim.SetBool("climbing", value);
		if (isClimbing) {
			body.gravityScale = 0;
		} else {
			SFXSpeak.Stop();
			body.gravityScale = gravityScaleDefault;
		}
	}

	public void giveItem(ItemType type, int id) {
		if (!isHolding) {
			itemHeld = (type, id);
			isHolding = true;
		}
	}

	public bool isHoldingItem((ItemType type, int id) item) {
		Debug.Log(itemHeld.Item1 + " " + itemHeld.Item2 + " " + item.type + " " + item.id);
		if (itemHeld.Item1 == item.type && itemHeld.Item2 == item.id) {
			return true;
		}
		return false;
	}

	public void clearItem() {
		if (isHolding) {
			itemHeld = (ItemType.Sunnata,999);
			isHolding = false;
		}
	}

	private void Awake()
	{
		// Grab references for rigidbody and animator from object
		body = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
		animationSpeed = anim.speed;
		gravityScaleDefault = body.gravityScale;

		// set up climbing audio
		SFXSpeak = GameObject.FindGameObjectWithTag("SFXSpeak").GetComponent<AudioSource>();
		SFXSpeak.clip = climbingClip;
		SFXSpeak.loop = true;
	}

	private void Update()
	{
		if (GameController.instance != null && !GameController.instance.gamePlaying) return;

		float horizontalInput = Input.GetAxis("Horizontal");
		body.velocity = new Vector2(horizontalInput * speed, body.velocity.y);

		if (isClimbing) {
			float verticalInput = Input.GetAxis("Vertical");
			// stop playing animation if not moving on the ladder
			if (verticalInput == 0) {
				PauseAnimation();
			} else {
				ContinueAnimation();
			}
			body.velocity = new Vector2(horizontalInput * speed, verticalInput * speed);
			if (verticalInput != 0 && !SFXSpeak.isPlaying)
			{
				SFXSpeak.Play();
			}
			else if (verticalInput == 0)
			{
				SFXSpeak.Pause();
			}
		} else {
			// make sure we continue the animation if we end in weird climbing state
			ContinueAnimation();
		}

		// Flip player when moving left-right
		if (horizontalInput > 0.01f)
		{
			transform.localScale = Vector3.one * playerScale;
		}
		else if (horizontalInput < -0.01f)
		{
			transform.localScale = new Vector3(-1, 1, 1) * playerScale;
		}

		if (Input.GetKeyDown(KeyCode.Space) && grounded)
		{
			Jump();
		}

		// Set animator parameters
		anim.SetBool("run", horizontalInput != 0);
		anim.SetBool("grounded", grounded);
	}

	private void Jump()
	{
		body.velocity = new Vector2(body.velocity.x, speed * 1.5f);
		anim.SetTrigger("jump");
		SFXSpeak.PlayOneShot(jumpingClip);
		grounded = false;
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "Ground")
		{
			grounded = true;
		}
	}

	private void PauseAnimation() {
		anim.speed = 0;
	}

	private void ContinueAnimation() {
		anim.speed = animationSpeed;
	}
}
