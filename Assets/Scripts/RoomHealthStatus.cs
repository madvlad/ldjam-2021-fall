using UnityEngine;

public class RoomHealthStatus : MonoBehaviour
{
	private SpriteRenderer spriteRenderer;
	[SerializeField] private Sprite health1;
	[SerializeField] private Sprite health2;
	[SerializeField] private Sprite health3;
	[SerializeField] private Sprite health4;

	private void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	public void UpdateHealthStatus(int health)
	{
		switch (health)
		{
			case 1:
				spriteRenderer.sprite = health1;
				break;
			case 2:
				spriteRenderer.sprite = health2;
				break;
			case 3:
				spriteRenderer.sprite = health3;
				break;
			case 4:
				spriteRenderer.sprite = health4;
				break;
		}

	}
}
