using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBehavior : MonoBehaviour
{
    public ItemType type;
    public int itemId;
    public AudioClip pickupClip;
    private AudioSource SFXSpeak;

    public void Reset() {
        this.gameObject.SetActive(true);
    }

    private void Start() {
        SFXSpeak = GameObject.FindGameObjectWithTag("SFXSpeak").GetComponent<AudioSource>();
    }

    /// <summary>
    /// Sent when another object enters a trigger collider attached to this
    /// object (2D physics only).
    /// </summary>
    /// <param name="other">The other Collider2D involved in this collision.</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player")){
            PlayerMovement player = other.gameObject.GetComponent<PlayerMovement>();
            if (!player.isHolding) {
                player.giveItem(type, itemId);
                if (pickupClip != null) {
                    SFXSpeak.PlayOneShot(pickupClip);
                }
                this.gameObject.SetActive(false);
            }
        }
    }
}
public enum ItemType {
    Sunnata,
    Pipe
}