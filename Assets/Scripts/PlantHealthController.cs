using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlantHealthController : MonoBehaviour
{
	public bool IsPlantPowered = true;
	public GameObject Lights;
	public GameObject Switch;
	public ItemBehavior[] PipePieces;
	public MissingPieceBehavior[] MissingPipePieces;
	public RodBehavior[] RodPieces;
	public ValveBehavior[] ValvePieces;

	private int pipeIndex = 0;
	private int rodIndex = 0;
	private int valveIndex = 0;
	[SerializeField] private Text roomHealthAlert;
	[SerializeField] private float timeInterval;
	[SerializeField] private int healthInterval;
	[SerializeField] private int redRoomHealth;
	[SerializeField] private int greenRoomHealth;
	[SerializeField] private int blueRoomHealth;
	[SerializeField] private int yellowRoomHealth;
	private RoomHealthStatus redHealthStatus, greenHealthStatus, blueHealthStatus, yellowHealthStatus;

	private void Start()
	{
		redHealthStatus = (RoomHealthStatus)GameObject.Find("redHealthStatus").GetComponent(typeof(RoomHealthStatus));
		greenHealthStatus = (RoomHealthStatus)GameObject.Find("greenHealthStatus").GetComponent(typeof(RoomHealthStatus));
		blueHealthStatus = (RoomHealthStatus)GameObject.Find("blueHealthStatus").GetComponent(typeof(RoomHealthStatus));
		yellowHealthStatus = (RoomHealthStatus)GameObject.Find("yellowHealthStatus").GetComponent(typeof(RoomHealthStatus));
		roomHealthAlert.enabled = false;
		InvokeRepeating("DecreaseRandomRoomHealth", timeInterval, timeInterval);
	}

	private void DecreaseRandomRoomHealth()
	{
		var rand = Random.Range(0f, 1f);
		string room;
		Color color;

		if (rand <= 0.25)
		{
			// Decrease Red Room Health
			redRoomHealth -= healthInterval;
			room = "ROD";
			color = Color.red;
			redHealthStatus.UpdateHealthStatus(redRoomHealth);
			if (rodIndex == 3) { rodIndex = 0; }
			RodPieces[rodIndex].Reset();
			rodIndex++;
		}
		else if (rand <= 0.5)
		{
			// Decrease Green Room Health
			greenRoomHealth -= healthInterval;
			room = "SMOKE";
			color = Color.green;
			greenHealthStatus.UpdateHealthStatus(greenRoomHealth);
			if (valveIndex == 3) { valveIndex = 0; }
			ValvePieces[valveIndex].Reset();
			valveIndex++;	
		}
		else if (rand <= 0.75)
		{
			// Decrease Blue Room Health
			blueRoomHealth -= healthInterval;
			room = "PIPE";
			color = Color.blue;
			blueHealthStatus.UpdateHealthStatus(blueRoomHealth);
			if (pipeIndex == 3) { pipeIndex = 0; }
			PipePieces[pipeIndex].Reset();
			MissingPipePieces[pipeIndex].Reset();
			pipeIndex++;
		}
		else
		{
			// Decrease Yellow Room Health
			yellowRoomHealth -= healthInterval;
			room = "ELECTRIC";
			color = Color.yellow;
			IsPlantPowered = false;
			Lights.SetActive(true);
			yellowHealthStatus.UpdateHealthStatus(yellowRoomHealth);
			Switch.GetComponent<SwitchBehavior>().SwitchOff();
		}

		if (redRoomHealth <= 0 || greenRoomHealth <= 0 || blueRoomHealth <= 0 || yellowRoomHealth <= 0)
		{
			GameController.instance.EndGame();
		}
		else
		{
			StartCoroutine(DisplayHealthAlert(room, color));
		}
	}

	private IEnumerator DisplayHealthAlert(string room, Color color)
	{
		roomHealthAlert.text = $"{room} ROOM HAS\nLOST STABILITY";
		roomHealthAlert.color = color;
		var count = 6;

		while (--count > 0)
		{
			roomHealthAlert.enabled = !roomHealthAlert.enabled;

			if (roomHealthAlert.enabled)
			{
				yield return new WaitForSeconds(1f);
			}
			else
			{
				yield return new WaitForSeconds(0.33f);
			}
		}

		roomHealthAlert.enabled = false;
	}

	public void IncreaseRoomHealth(string room)
	{
		switch (room.ToLower())
		{
			case "red":
				redRoomHealth += healthInterval;
				redHealthStatus.UpdateHealthStatus(redRoomHealth);
				break;
			case "green":
				greenRoomHealth += healthInterval;
				greenHealthStatus.UpdateHealthStatus(greenRoomHealth);
				break;
			case "blue":
				blueRoomHealth += healthInterval;
				blueHealthStatus.UpdateHealthStatus(blueRoomHealth);
				break;
			case "yellow":
				yellowRoomHealth = 4;
				Lights.SetActive(false);
				IsPlantPowered = true;
				yellowHealthStatus.UpdateHealthStatus(yellowRoomHealth);
				break;
		}
	}
}
