using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissingPieceBehavior : MonoBehaviour
{
    public int RequiredId;
    public ItemType RequiredType;
    public AudioClip FixSound;
    public GameObject FixedPiece;
    public PlantHealthController GameController;
    private AudioSource SFXSpeak;

    public void Reset() {
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
        gameObject.GetComponent<SpriteRenderer>().enabled = true;
        FixedPiece.GetComponent<SpriteRenderer>().enabled = false;
    }

    private void Start() {
        SFXSpeak = GameObject.FindGameObjectWithTag("SFXSpeak").GetComponent<AudioSource>();
    }
    
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.CompareTag("Player")) {
            var player = other.gameObject.GetComponent<PlayerMovement>();
            if (player.isHoldingItem((this.RequiredType, this.RequiredId))) {
                player.clearItem();
                SFXSpeak.PlayOneShot(FixSound);
                //Destroy(gameObject);
                gameObject.GetComponent<BoxCollider2D>().enabled = false;
                gameObject.GetComponent<SpriteRenderer>().enabled = false;
                FixedPiece.GetComponent<SpriteRenderer>().enabled = true;
                GameController.IncreaseRoomHealth("blue");
            }
        }
    }
}
