using UnityEngine;

public class PlayerMovementWithWallJump : MonoBehaviour
{
	public bool isClimbing = false;
	public bool isHolding = false;

	[SerializeField] private float speed;
	[SerializeField] private float jumpPower;
	[SerializeField] private LayerMask groundLayer;
	[SerializeField] private LayerMask wallLayer;
	private Rigidbody2D body;
	private Animator anim;
	private BoxCollider2D boxCollider;
	private float gravityScaleDefault;
	private (ItemType, int) itemHeld;
	private float wallJumpCooldown;
	private float horizontalInput;

	public void setClimbing(bool value)
	{
		isClimbing = value;
		if (isClimbing)
		{
			body.gravityScale = 0;
		}
		else
		{
			body.gravityScale = gravityScaleDefault;
		}
	}

	public void giveItem(ItemType type, int id)
	{
		if (!isHolding)
		{
			itemHeld = (type, id);
			isHolding = true;
		}
	}

	public bool isHoldingItem((ItemType type, int id) item)
	{
		Debug.Log(itemHeld.Item1 + " " + itemHeld.Item2 + " " + item.type + " " + item.id);
		if (itemHeld.Item1 == item.type && itemHeld.Item2 == item.id)
		{
			return true;
		}
		return false;
	}

	private void Awake()
	{
		// Grab references for rigidbody and animator from object
		body = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
		boxCollider = GetComponent<BoxCollider2D>();
		gravityScaleDefault = body.gravityScale;
	}

	private void Update()
	{
		if (!GameController.instance.gamePlaying) return;

		horizontalInput = Input.GetAxis("Horizontal");
		//body.velocity = new Vector2(horizontalInput * speed, body.velocity.y);

		if (isClimbing)
		{
			float verticalInput = Input.GetAxis("Vertical");
			body.velocity = new Vector2(horizontalInput * speed, verticalInput * speed);
		}

		// Flip player when moving left-right
		if (horizontalInput > 0.01f)
		{
			transform.localScale = Vector3.one;
		}
		else if (horizontalInput < -0.01f)
		{
			transform.localScale = new Vector3(-1, 1, 1);
		}


		// Set animator parameters
		anim.SetBool("run", horizontalInput != 0);
		anim.SetBool("grounded", IsGrounded());

		// Wall jump logic
		if (wallJumpCooldown > 0.2f)
		{
			body.velocity = new Vector2(horizontalInput * speed, body.velocity.y);

			if (OnWall() && !IsGrounded())
			{
				body.gravityScale = 0;
				body.velocity = Vector2.zero;
			}
			else
			{
				body.gravityScale = 7;
			}

			if (Input.GetKey(KeyCode.Space))
			{
				Jump();
			}
		}
		else
		{
			wallJumpCooldown += Time.deltaTime;
		}
	}

	private void Jump()
	{
		if (IsGrounded())
		{
			body.velocity = new Vector2(body.velocity.x, jumpPower);
			anim.SetTrigger("jump");
		}
		else if (OnWall() && !IsGrounded())
		{
			if (horizontalInput == 0)
			{
				body.velocity = new Vector2(-Mathf.Sign(transform.localScale.x) * 10, 0);
				transform.localScale = new Vector3(-Mathf.Sign(transform.localScale.x), transform.localScale.y, transform.localScale.z);
			}
			else
			{
				body.velocity = new Vector2(-Mathf.Sign(transform.localScale.x) * 3, 6);
			}

			wallJumpCooldown = 0;
		}
	}

	private bool IsGrounded()
	{
		RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider.bounds.center, boxCollider.bounds.size - new Vector3(0.01f, 0), 0, Vector2.down, 0.1f, groundLayer);
		return raycastHit.collider != null;
	}

	private bool OnWall()
	{
		//RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider.bounds.center, boxCollider.bounds.size, 0, new Vector2(transform.localScale.x, 0), 0.1f, wallLayer);
		//return raycastHit.collider != null;
		return false;
	}
}
