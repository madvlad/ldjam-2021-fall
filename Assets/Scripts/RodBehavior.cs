using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RodBehavior : MonoBehaviour
{
    public GameObject rodObject;
    public AudioClip BopClip;
    private AudioSource SFXSpeak;
    private PlantHealthController PlantHealth;

    public void Reset() {
        gameObject.SetActive(true);
        rodObject.SetActive(true);
    }

    private void Start() {
        SFXSpeak = GameObject.FindGameObjectWithTag("SFXSpeak").GetComponent<AudioSource>();
        PlantHealth = GameObject.FindGameObjectWithTag("GameManager").GetComponent<PlantHealthController>();
    }

    /// <summary>
    /// Sent when another object enters a trigger collider attached to this
    /// object (2D physics only).
    /// </summary>
    /// <param name="other">The other Collider2D involved in this collision.</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if (PlantHealth.IsPlantPowered && other.gameObject.CompareTag("Player")) {
            SFXSpeak.PlayOneShot(BopClip);
            PlantHealth.IncreaseRoomHealth("red");
            rodObject.SetActive(false);
        }
    }
}
